import os, time, requests, base64
path_to_watch = "./photos"
before = dict ([(f, None) for f in os.listdir (path_to_watch)])
# url = "http://localhost:5000/api/events"
url = "http://gallery.theheartbooth.com/api/events"

# EDIT for each party
title = "Test Party #2"
description = "#yolo?"
location = "San Diego, CA"

photoID =  title.replace(" ", "-") + "-"
photoIDNum = 0001

# Run loop
while 1:
	time.sleep (2)
	after = dict ([(f, None) for f in os.listdir (path_to_watch)])
	added = [f for f in after if not f in before]
	removed = [f for f in before if not f in after]
	
	# 	Check for added photo
	if added:
		
		print added
		
		for index, photo in enumerate(added):
		
			photoPath = path_to_watch + "/" + added[index]
					
			with open(photoPath, "rb") as image_file:
				encoded_string = base64.b64encode(image_file.read())
	
				newEvent = True
				
				# Loop through partyEvents to find a party with current party title
				r = requests.get(url)
				for partyEvent in r.json():
					
					if partyEvent["title"] == title:
						
						newEvent = False
						
						print "Uploading ", added[index], " to ", partyEvent["title"]
						
						payload = {
							"title": partyEvent["title"],
							"description": partyEvent["description"],
							"location": partyEvent["location"],
							"photo": encoded_string,
							"photoID": photoID + str(photoIDNum)
							# "id": partyEvent["_id"]
						}
						
						eventURL = url + "/" + partyEvent["_id"]
						r = requests.put(eventURL, data=payload)
											
						if r.status_code == 200:
							print "success!"
							photoIDNum += 1
				
				if newEvent:
					
					print "Uploading ", (added[index]), "to ", title
					
					payload = {
						"title": title,
						"description": description,
						"location": location,
						"photo": encoded_string,
						"photoID": photoID + str(photoIDNum),
					}
					
					r = requests.post(url, data=payload)
					
					if r.status_code == 200:
						print "success!"
						photoIDNum += 1
			
				print "Added: ", (added[index])
	
	if removed: print "Removed: ", ", ".join (removed)
	
	before = after
